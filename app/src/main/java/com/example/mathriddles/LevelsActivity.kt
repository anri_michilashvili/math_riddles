package com.example.mathriddles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_levels.*
import kotlinx.android.synthetic.main.activity_question.*

class LevelsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_levels)
        init()
    }
    fun init() {
        level2.isEnabled=false
        level3.isEnabled = false
        level4.isEnabled = false
        level5.isEnabled = false
        level6.isEnabled = false
        level7.isEnabled = false
        level8.isEnabled = false
        level9.isEnabled = false

        val intent = Intent(this@LevelsActivity, QuestionActivity::class.java)
        level1.setOnClickListener {
            intent.putExtra("questionnum", 1)
            startActivityForResult(intent, 10)
        }

        level2.setOnClickListener{
            intent.putExtra("questionnum", 2)
            startActivityForResult(intent,20)
        }
        level3.setOnClickListener{
            intent.putExtra("questionnum", 3)
            startActivityForResult(intent,30)
        }
        level4.setOnClickListener{
            intent.putExtra("questionnum", 4)
            startActivityForResult(intent,40)
        }
        level5.setOnClickListener{
            intent.putExtra("questionnum", 5)
            startActivityForResult(intent,50)
        }
        level6.setOnClickListener{
            intent.putExtra("questionnum", 6)
            startActivityForResult(intent,60)
        }
        level7.setOnClickListener{
            intent.putExtra("questionnum", 7)
            startActivityForResult(intent,70)
        }
        level8.setOnClickListener{
            intent.putExtra("questionnum", 8)
            startActivityForResult(intent,80)
        }
        level9.setOnClickListener{
            intent.putExtra("questionnum", 9)
            startActivityForResult(intent,90)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == RESULT_OK && requestCode==10)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level2.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==20)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level3.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==30)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level4.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==40)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level5.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==50)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level6.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==60)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level7.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==70)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level8.isEnabled=true
        }
        if(resultCode == RESULT_OK && requestCode==80)  {
            val questionNum = data!!.extras!!.getInt("questionnum",0)
            level9.isEnabled=true
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
