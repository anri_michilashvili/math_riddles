package com.example.mathriddles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        anim()
        singup_button.setOnClickListener {
            startActivity(Intent(this,SignUp::class.java))
        }

        auth = Firebase.auth
      login_button.setOnClickListener {
          signIn()
      }
    }
    private fun signIn(){
        val email:String = email.text.toString()
        val password = password.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty()){
            progressBarLogin.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressBarLogin.visibility = View.GONE
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("signIn", "signInWithEmail:success")
//
                        startActivity(Intent(this,LevelsActivity::class.java))

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("signIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "User Authentication Failed:"+task.exception?.message,
                            Toast.LENGTH_LONG).show()
                    }

                }


        }else{
            Toast.makeText(this,"Please fill all fields", Toast.LENGTH_SHORT).show()}
    }


    private fun anim(){
        val ttb= AnimationUtils.loadAnimation(this, R.anim.ttb)
        val for_log_in= AnimationUtils.loadAnimation(this, R.anim.for_log_in)
        val for_sign_up= AnimationUtils.loadAnimation(this, R.anim.for_sign_up)

        email.startAnimation(ttb)
        password.startAnimation(ttb)
        singup_button.startAnimation(for_sign_up)
        login_button.startAnimation(for_log_in)
    }
}
