package com.example.mathriddles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pop_up_pyth.*

class PopUpPyth : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up_pyth)
        init()
    }
    fun init()
    {
        closebtn.setOnClickListener{
            setResult(RESULT_OK, intent)
            finish()
        }
    }
}