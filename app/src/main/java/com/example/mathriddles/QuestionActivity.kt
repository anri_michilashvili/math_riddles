package com.example.mathriddles
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_levels.*
import kotlinx.android.synthetic.main.activity_question.*


class QuestionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
        init()
    }
    fun init() {
        val qnummer = intent.getIntExtra("questionnum", 0)

        if (qnummer == 1) {
            questiontext.setText("what is 5x5?")
            hinttext.setText("multiplication")
            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "25") {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    setResult(RESULT_OK, intent)
                    finish()
                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 2) {
            questiontext.setText("If two fair coins are flipped, what is the probability that one will come up heads and the other tails?")
            hinttext.setText("Think of the coins as a penny and a dime, and list all possibilities.")

            checkanswer.setOnClickListener {
                if ((answertext.text.toString() == "1/2") || (answertext.text.toString() == "2/4")) {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }

        } else if (qnummer == 3) {
            questiontext.setText("what is 1/20 as precentage?")
            hinttext.setText("divide 100 by 20")

            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "5%") {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 4) {
            questiontext.setText("How many kms is 2764dm?")
            hinttext.setText("1dm is 10cm")

            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "0.2764") {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 5) {
            questiontext.setText("Simplify: 2c+5d-7c+2d")
            hinttext.setText("find the similar values")

            checkanswer.setOnClickListener {
                val intent1 = Intent(this@QuestionActivity, PopUpPyth::class.java)
                if (answertext.text.toString() == "-5c+7d") {
                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    intent.putExtra("questionnum", 1)
                    startActivity(intent1)
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 6) {
            questiontext.setText("what is 2⁻³?")
            hinttext.setText("negative power reverses the number")

            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "1/8") {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    intent.putExtra("questionnum", 1)
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 7) {
            questiontext.setText("What is the sqaure root of 289??")
            hinttext.setText("what times what is 289?")

            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "17") {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    intent.putExtra("questionnum", 1)
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 8) {
            questiontext.setText("Evaluate f(2) - f(1), if f(x) = 6x + 1")
            hinttext.setText("Put the numbers where X is")

            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "6") {

                    Toast.makeText(applicationContext, "Correct", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    intent.putExtra("questionnum", 1)
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        } else if (qnummer == 9) {
            questiontext.setText("|- 2 x + 2| - 3 = -3")
            hinttext.setText("Add 3 to both sides of the equation and simplify.")

            checkanswer.setOnClickListener {
                if (answertext.text.toString() == "1") {

                    Toast.makeText(applicationContext, "Correct, You've Finished this game", Toast.LENGTH_SHORT).show()
                    val intent = intent
                    intent.putExtra("questionnum", 1)
                    setResult(RESULT_OK, intent)
                    finish()

                } else {
                    Toast.makeText(applicationContext, "Incorrect", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        }
    }
}
